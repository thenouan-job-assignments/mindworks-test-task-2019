> 
> 
> Exercise is to write an implementation which, given the graph and two nodes, would return all possible paths from first node to another. For graph 1->2, 1->3, 2->4, 3->4 implementation would return [[1,2,4], [1,3,4]] if 1 and 4 are passed as two nodes. 
> 
> Interface I expect:
> 
> # this can be in class, module, a class method, whatever, your choice
> def paths_between(node1, node2, edge_pairs)
>   # your implementation
> end
> 
> # When i test
> paths_between(1, 4, [[1,2], [1,3], [2,4], [3, 4]])
> # i should get [[1,2,4], [1,3,4]]
> 
> Of course, you should also handle the edge cases (pun intended).
> 
> 
> 
> 
> 
