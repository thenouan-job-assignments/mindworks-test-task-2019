def paths_between(node1, node2, edge_pairs)
  if node1 == node2
    return [[node1]]
  end

  paths = []
  for edge1, edge2 in edge_pairs do
    if edge1 == node1
      paths += paths_between(edge2, node2, edge_pairs).map {|x| [edge1] + x}
    end
  end

  paths
end