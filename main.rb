require './paths_between.rb'

def test(node1, node2, edge_pairs)
  puts '-' * 15
  puts 'node1: ' + node1.to_s
  puts 'node2: ' + node2.to_s
  puts 'graph: ' + edge_pairs.to_s
  puts 'result: ' + paths_between(node1, node2, edge_pairs).to_s
end

test(1, 4, [[1,2], [1,3], [2,4], [3,4]])
test(1, 20, [[1,2], [1,3], [2,4], [3,4]])
test(10, 20, [[1,2], [1,3], [2,4], [3,4]])
test(3, 4, [[1,2], [1,3], [2,4], [3,4]])
test(1, 4, [[1,2], [1,3], [2,5], [5,6], [2,4], [3,4]])
test(1, 4, [[1,2], [1,3], [2,5], [5,4], [2,4], [3,4]])

# Cycle
# test(1, 3, [[1,2], [2,1], [2, 3]])
